document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('registro-form');

    form.addEventListener('submit', function (e) {
        e.preventDefault();

        const nombre = document.getElementById('nombre').value;
        const email = document.getElementById('email').value;
        const contrasena = document.getElementById('contrasena').value;
        const discapacidad = document.getElementById('discapacidad').value;
        const fechanacimiento = document.getElementById('fechanacimiento').value;
        const ci = document.getElementById('ci').value;
        const carrera = document.getElementById('carrera').value;
        const telefono = document.getElementById('telefono').value;
        const userType = document.getElementById("user-type").value;
        const genero = document.getElementById("genero").value;

        const usuario = {
            nombre: nombre,
            email: email,
            contrasena: contrasena,
            discapacidad: discapacidad,
            fechanacimiento: fechanacimiento,
            ci: ci,
            carrera: carrera,
            telefono: telefono,
            userType: userType,
            genero: genero

        };

        localStorage.setItem('usuario', JSON.stringify(usuario));
        form.reset();

        alert('Usuario registrado con éxito.');
    });
});
const imagenUsuario = document.getElementById('imagen-usuario');

document.addEventListener('DOMContentLoaded', function () {
    const loginForm = document.getElementById('login-form');
    const mensaje = document.createElement('div');
    mensaje.style.marginTop = '10px';

    loginForm.addEventListener('submit', function (e) {
        e.preventDefault();

        const email = document.getElementById('email').value;
        const contrasena = document.getElementById('contrasena').value;

        const usuarioGuardado = JSON.parse(localStorage.getItem('usuario'));

        if (usuarioGuardado && usuarioGuardado.email === email && usuarioGuardado.contrasena === contrasena) {
            mostrarMensaje('Inicio de sesión exitoso', 'green');
            window.location.href = 'home.html';
        } else {
            mostrarMensaje('Error: Correo no registrado o contraseña incorrecta', 'red');
        }
    });

    function mostrarMensaje(texto, color) {
        mensaje.textContent = texto;
        mensaje.style.color = color;
        loginForm.appendChild(mensaje);
        setTimeout(function () {
            mensaje.textContent = '';
        }, 3000);
    }
});

document.addEventListener('DOMContentLoaded', () => {
    const datosUsuario = JSON.parse(localStorage.getItem('usuario'));

    if (datosUsuario) {
        const datosUsuarioDiv = document.getElementById('datos-usuario');
        datosUsuarioDiv.innerHTML = `
            <h2>${datosUsuario.nombre}</h2>
            <p><strong>Email:</strong> ${datosUsuario.email}</p>
            <p><strong>Tipo de usuario:</strong> ${datosUsuario.tipo}</p>
            <p><strong>Género:</strong> ${datosUsuario.genero}</p>
            <p><strong>Cédula de Identidad:</strong> ${datosUsuario.ci}</p>
            <p><strong>Número de Teléfono:</strong> ${datosUsuario.telefono}</p>
            <p><strong>Discapacidad:</strong> ${datosUsuario.discapacidad}</p>
            <p><strong>Fecha de Nacimiento:</strong> ${datosUsuario.fechanacimiento}</p>
            <p><strong>Carrera:</strong> ${datosUsuario.carrera}</p>
        `;
    } else {
        const datosUsuarioDiv = document.getElementById('datos-usuario');
        datosUsuarioDiv.innerHTML = 'No se encontraron datos de usuario en el almacenamiento local.';
    }
});

document.addEventListener('DOMContentLoaded', () => {
    let slideIndex = 0;
    mostrarSlide(slideIndex);

    document.getElementById('anterior').addEventListener('click', () => cambiarSlide(-1));
    document.getElementById('siguiente').addEventListener('click', () => cambiarSlide(1));

    function cambiarSlide(n) {
        mostrarSlide(slideIndex += n);
    }

    function mostrarSlide(n) {
        const slides = document.getElementsByClassName('slide');
        if (n >= slides.length) {
            slideIndex = 0;
        }
        if (n < 0) {
            slideIndex = slides.length - 1;
        }
        for (let i = 0; i < slides.length; i++) {
            slides[i].style.display = 'none';
        }
        slides[slideIndex].style.display = 'block';
    }
    setInterval(() => cambiarSlide(1), 5000);
});

document.addEventListener('DOMContentLoaded', () => {
    const datosUsuario = JSON.parse(localStorage.getItem('usuario'));

    if (datosUsuario) {
        document.getElementById('nombre').value = datosUsuario.nombre;
        document.getElementById('email').value = datosUsuario.email;
        document.getElementById('discapacidad').value = datosUsuario.discapacidad;
        document.getElementById('telefono').value = datosUsuario.telefono;
        
        const editProfileForm = document.getElementById('edit-profile-form');

        editProfileForm.addEventListener('submit', function (event) {
            event.preventDefault();
            const nombre = document.getElementById('nombre').value;
            const email = document.getElementById('email').value;
            const discapacidad = document.getElementById('discapacidad').value;
            const telefono = document.getElementById('telefono').value;
            datosUsuario.nombre = nombre;
            datosUsuario.email = email;
            datosUsuario.discapacidad = discapacidad;
            datosUsuario.telefono = telefono;

            localStorage.setItem('usuario', JSON.stringify(datosUsuario));

            window.location.href = 'consultadatos.html';
        });
    } else {
        alert('No se encontraron datos de usuario en el almacenamiento local.');
    }
});

document.addEventListener('DOMContentLoaded', function () {
    const solicitudForm = document.getElementById('solicitud-form');
    const mensajeExito = document.getElementById('mensaje-exito');

    solicitudForm.addEventListener('submit', function (e) {
        e.preventDefault();
        const nombresol = document.getElementById('nombre').value;
        const descripcion = document.getElementById('descripcion').value;
        
        const documento = document.getElementById('documento').files[0];

        const solicitudseg = {
            nombresol: nombresol,
            descripcion: descripcion,
            documento: documento
        };

        localStorage.setItem('solicitudseg', JSON.stringify(solicitudseg));
        solicitudForm.reset();

        mensajeExito.textContent = 'Solicitud enviada con éxito. ¡Gracias!';
        mensajeExito.style.color = 'green';
        mensajeExito.style.display = 'block';

        window.location.href = 'home.html';
    });
});